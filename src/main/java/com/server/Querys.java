package com.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Querys {
	
	Connection con;
	public Querys() {
		// TODO Auto-generated constructor stub
	}
	
	public void insertSenha(int id, int senha, boolean status, String tipo) {

		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
		
		String sql = "INSERT INTO senhas (id, senha, status, tipo) VALUES (?, ?, ?, ?)";
		
		PreparedStatement statement = null;
		try {
			statement = con.prepareStatement(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			statement.setInt(1, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			statement.setInt(2, senha);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			statement.setBoolean(3, status);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			statement.setString(4, tipo);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		int rowsInserted = 0;
		try {
			rowsInserted = statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public int selectSenhaPrior(){
		
		int senhaPrior = 0;
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
		
		Statement statement = null;
	    try {
		
			statement = con.createStatement();
	        ResultSet rs = statement.executeQuery( "SELECT senha  FROM senhas where tipo = 'P' and id = (select max(id) from senhas where tipo = 'P');" );
	        
	        if (rs.next() == true) {
	        	
	        	senhaPrior = rs.getInt("senha");
	        }
	        else // N�o existe senha
	        {
	        	senhaPrior = 0;
	        }
	        rs.close();
	        statement.close();
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
	    
    
		return senhaPrior;
		
	}
	
	public int selectSenhaNormal(){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
		
		int senhaNormal = 0;
		
		Statement statement = null;
	    try {
		
			statement = con.createStatement();
	        ResultSet rs = statement.executeQuery( "SELECT senha  FROM senhas where tipo = 'N' and id = (select max(id) from senhas where tipo = 'N');" );
			
	        if (rs.next() == true) {
				
	        	senhaNormal = rs.getInt("senha");
	        }
	        else // N�o existe senha
	        {
	        	senhaNormal = 0;
	        }
	        rs.close();
	        statement.close();
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
		return senhaNormal;
		
	}

	public String selectChamarSenha(){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
	    
		String senhaString = "";
		int senha = 0;
		
		Statement statement = null;
		Statement statement2 = null;
		Statement statement3 = null;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		PreparedStatement pst3 = null;
		PreparedStatement pst4 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		

	    try {
		
			statement = con.createStatement();
			
			// Alterar para verdadeiro a senha que foi chamada anteriormente
    	    String sql1 = "UPDATE senhas SET senhachamada = false WHERE senhachamada = true;";
            pst2 = con.prepareStatement(sql1);
            pst2.executeUpdate();
            
	        rs = statement.executeQuery( "SELECT count(*) count FROM senhas where tipo = 'P' and status = 'f';" );
	        rs.next();
	        	        
	        if (rs.wasNull() == false) {
	        	
	        	if (rs.getInt("count") >= 1)  { // encontrou priorit�rio

			        statement2 = con.createStatement();
			        rs2 = statement2.executeQuery( "SELECT min(senha) as senha FROM senhas where tipo = 'P' and status = 'f';" );
			        rs2.next();

		        	senha = rs2.getInt("senha");
		    	    senhaString = "P"+String.format("%04d", senha);
		    	    
		    	    // alterar no banco o status
		    	    String sql = "UPDATE senhas SET status = ? , senhachamada = ? where tipo = 'P' and senha = ?;";
		    	    
		            pst = con.prepareStatement(sql);
		            pst.setBoolean(1, true);
		            pst.setBoolean(2, true);
		            pst.setInt(3, senha);
		            
		            pst.executeUpdate();
	        	}
	        	else {
    	        	
    	        	rs3 = statement.executeQuery( "SELECT count(*) as count FROM senhas where tipo = 'N' and status = 'f';" );
    	        	rs3.next();
    	        	
    		        if (rs3.wasNull() == false) { // foi encontrada senha
    		        	if (rs3.getInt("count") >= 1) { 
    						statement3 = con.createStatement();
    				        rs4 = statement3.executeQuery( "SELECT min(senha) as senha FROM senhas where tipo = 'N' and status = 'f';" );
    				        rs4.next();
	    		        	
	    		        	senha = rs4.getInt("senha");
	    		    	    senhaString = "N"+String.format("%04d", senha);
	    		    	    
	    		    	    // alterar no banco o status
	    		    	    String sql = "UPDATE senhas SET status = ? , senhachamada = ? where tipo = 'N' and senha =  ?;";
	    		    	    
	    		            pst4 = con.prepareStatement(sql);
	    		            pst4.setBoolean(1, true);
	    		            pst4.setBoolean(2, true);
	    		            pst4.setInt(3, senha);                   
	    		            pst4.executeUpdate();
    		        	}
        		        else
        		        {
        		        	senhaString = "NO"; // n�o foi encontrada senha dispon�vel
        		        }
    		        }
    		        else
    		        {
    		        	senhaString = "NO"; // n�o foi encontrada senha dispon�vel
    		        }
	        	}
	        }
	        else {
	        	senhaString = "NO"; // n�o foi encontrada senha dispon�vel
	        }
	        
	        if (rs != null) {
	        	rs.close();
	        }
	        if (rs2 != null) {
	        	rs2.close();
	        }
	        if (rs3 != null) {
	        	rs3.close();
	        }
	        if (rs4 != null) {
	        	rs4.close();
	        }
	        if (statement != null) {
	        	statement.close();
	        }
	        if (statement2 != null) {
	        	statement2.close();
	        }
	        if (statement3 != null) {
	        	statement3.close();
	        }
	        if (pst != null) {
                pst.close();
            }
	        if (pst2 != null) {
                pst2.close();
            }
	        if (pst3 != null) {
                pst3.close();
            }
	        if (pst4 != null) {
                pst4.close();
            }
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
	    
		return senhaString;
		
	}	
	
	public int selectId(){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
	    
		int id = 0;
		
		Statement statement = null;
	    try {
		
			statement = con.createStatement();
	        ResultSet rs = statement.executeQuery( "SELECT max(id) as id FROM senhas;" );
	        rs.next();
	        
	        if (rs.wasNull() == false) {
	        	
	        	id = rs.getInt("id");
	        }
	        else // N�o existe id
	        {
	        	id = 0;
	        }

	        rs.close();
	        statement.close();
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
		return id;
		
	}
	
	public String selectPainel(){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
		
		int senhaPainel = 0;
		String senhaString = "";
		
		Statement statement = null;
		Statement statement2 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		
	    try {
		
			statement = con.createStatement();
	        rs = statement.executeQuery( "SELECT count(*) as count  FROM senhas where senhachamada = true;" );
	        rs.next();
	        if (rs.wasNull() == false) {
	        	if (rs.getInt("count") == 1) { 
					statement2 = con.createStatement();
			        rs2 = statement2.executeQuery( "SELECT senha, tipo  FROM senhas where senhachamada = true;" );
			        rs2.next();
		        	senhaPainel = rs2.getInt("senha");
		    	    senhaString = rs2.getString("tipo") + String.format("%04d", senhaPainel);
	        	}
	        	
	        }
	        else
	        {
	        	senhaString = "NO"; // n�o foi encontrada senha dispon�vel
	        }

	        if (rs!= null) {
	        	rs.close();
	        }
	        if (rs2!= null) {
	        	rs2.close();
	        }
	        if (statement != null) {
	        	statement.close();
	        }
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
		return senhaString;
		
	}
	

	public void reiniciarSenhas(){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
	    
		Statement statement = null;

	    try {
		
			statement = con.createStatement();
			
    	    String sql = "DELETE from senhas;";
    	    
            statement.executeUpdate(sql);
			
	        statement.close();
	        
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
	    
	}
	
	public void excluiSenhas(String tipo){
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();
	    
		PreparedStatement pst = null;

	    try {
		
			
    	    String sql = "DELETE from senhas WHERE tipo = ? and status = true and senhachamada = false;";
    	    
            pst = con.prepareStatement(sql);
            pst.setString(1, tipo);
            
            pst.executeUpdate();    	    
    	    
	        if (pst != null) {
                pst.close();
            }
	        
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }
	    
	}

	
	public boolean selectExist(String tipo, int senha){
		

		boolean result = false;
		
		// Conex�o com o Banco
		Conexao conect = new Conexao();
		
	    con = conect.conexao();


		PreparedStatement pst = null;
		
		
	    try {

	        String sql = "SELECT count(*) count  FROM senhas where tipo = ? and senha = ?;";
	        
            pst = con.prepareStatement(sql);
            pst.setString(1, tipo);
            pst.setInt(2, senha);
			
	        ResultSet rs = pst.executeQuery();
	        if (rs.wasNull() == false) {
	        	rs.next();
	        	if (rs.getInt("count") == 0) { 
	        		result = false;
	        	}
	        	else {
	        		result = true;
	        	}
	        }
	        else
	        {
	        	result = false; // n�o foi encontrada senha dispon�vel
	        }	        

	        rs.close();
	        if (pst != null) {
	            pst.close();
	        }
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    } catch ( Exception e ) {
	        System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	        System.exit(0);
	    }

	    return result;
		
	}
	

}
