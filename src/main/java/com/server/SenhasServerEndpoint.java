package com.server;

import java.io.IOException;

import javax.swing.JOptionPane;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/senhasServerEndpoint")
public class SenhasServerEndpoint {
	private int pwdPrior = 0;
	private int pwdNormal = 0;
	private String senha = "";
	
	int id = 0;

	Querys querys = new Querys();
	
	String senhaChamada = "";
	String senhaPainel = "";
	
	@OnOpen
	public void handleOpen(Session userSession) {

	}

	@OnMessage
	public void handleMessage(String message, Session userSession) throws IOException {
		boolean existe = false;
		
		// Gerar Senha Priorit�ria
        if ("P".equals(message)) {
			
			pwdPrior = querys.selectSenhaPrior();			
			pwdPrior = pwdPrior + 1;

			if (pwdPrior > 9999) {
				querys.excluiSenhas("P");
				pwdPrior = 1;
			}
			
			id = querys.selectId();			
			id = id + 1;
			// Verifica se existe a senha no banco, no caso de existir n�o incluir
			
			existe = querys.selectExist("P", pwdPrior);
			
			if (existe == false) {
				// false = ainda n�o foi chamada
				querys.insertSenha(id, pwdPrior, false, "P");
			}
			
			
			senha = String.format("%04d", pwdPrior);
			userSession.getBasicRemote().sendText("P"+senha);
		}
        // Gerar senha normal
        else if ("N".equals(message)) {
        	
			pwdNormal = querys.selectSenhaNormal();			
			pwdNormal = pwdNormal + 1;

			if (pwdNormal > 9999) {
				querys.excluiSenhas("N");
				pwdNormal = 1;
			}

			
			id = querys.selectId();			
			id = id + 1;
			
			// Verifica se existe a senha no banco, no caso de existir n�o incluir
			
			existe = querys.selectExist("N", pwdNormal);
			
			if (existe == false) {
				// false = ainda n�o foi chamada
				querys.insertSenha(id, pwdNormal, false, "N");
			}
			senha = String.format("%04d", pwdNormal);
			userSession.getBasicRemote().sendText("N"+senha);
        }
        // Chamar prox. senha
        else if ("G".equals(message)) {
			
			senhaChamada = querys.selectChamarSenha();
        	
			userSession.getBasicRemote().sendText(senhaChamada);
        }
        else if ("R".equals(message)) {
        	
        	querys.reiniciarSenhas();
        	
			userSession.getBasicRemote().sendText(" ");
        }
        else if ("L".equals(message)) {
        	
        	senhaPainel = querys.selectPainel();
        	
			userSession.getBasicRemote().sendText(senhaPainel);
        }
        
	}
	
	@OnClose
	public void handleClose(Session userSession){

		
	}

}
